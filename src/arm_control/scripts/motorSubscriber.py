#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from gotoCoordinate import gotoCoordinate
from RosNode import RosNode
import numpy as np
import time

def callback(data):

    #logging and printing to terminal:
    fromString = np.fromstring( data.data, int )
    print( "From camera: ", fromString)
    #rospy.loginfo(data)
    gotoCoordinate( fromString[ 0 ], fromString[ 1 ], fromString[ 2 ] )
    gotoCoordinate(30, 0, 20)
    gotoCoordinate(30,0,30)
    armNode.publish(armNode.pub1, "done")



if __name__ == '__main__':
    #time.sleep(15)
    print("motorSubscriber ready")
    armNode = RosNode(nodeName="armNode", subscribeTopic1="motorPositions", callback1=callback, publishTopic1="doneMoving")
    armNode.initNode()
    armNode.subcribeAndWait(armNode.SubscribeTopic1)
