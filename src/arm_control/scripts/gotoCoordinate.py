
import rospy
import actionlib
from control_msgs.msg import FollowJointTrajectoryAction
from control_msgs.msg import FollowJointTrajectoryFeedback
from control_msgs.msg import FollowJointTrajectoryResult
from control_msgs.msg import FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint
from trajectory_msgs.msg import JointTrajectory
from invkin import invkin
import math

def gotoCoordinate( x, y, z ):

     #followpointTrajectory needs an array (tuple)
    joint_positions = []

    #The client name. TODO: figure out!
    server_name = "/arm_controller/follow_joint_trajectory"
    
    #Same as above:
    client = actionlib.SimpleActionClient(server_name, FollowJointTrajectoryAction)
    joint_names=["joint1","joint2","joint3","joint4"]
    
    xyz = [
    [x, y, z]
    ]

    dur = rospy.Duration(1)
    

    #TrajectoryPoints only ones
    for p in xyz:
        jtp = JointTrajectoryPoint(positions=invkin(p),velocities=[0.1]*4,time_from_start=dur)
        dur += rospy.Duration(2)
        joint_positions.append(jtp)

        
    #making a trajectory of the points:
    jt = JointTrajectory( joint_names=joint_names, points=joint_positions)
    
    #TrajectoryGoal
    jtg = FollowJointTrajectoryGoal(trajectory=jt, goal_time_tolerance=dur+rospy.Duration(2))

    client.wait_for_server()
    
    client.send_goal(jtg)

    client.wait_for_result()
