#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from RosNode import RosNode
import numpy as np
import time

pub = rospy.Publisher('MotorPositions', String, queue_size=10)
hello_str = "hello world"


subscribing = "Going to subscribe"
publishing = "Going to publish"

def callbackFromDoneMoving(data):
    cameraNode.sub1.unregister()


    #logging and printing to terminal:
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    rospy.loginfo(data)
    rospy.loginfo("receivedDoneMoving")
    print(cameraNode.pub2.lastVal)
    if(cameraNode.pub2.lastVal == 0):
        cameraNode.publish(cameraNode.pub2, 0.85)
    else:
        cameraNode.publish(cameraNode.pub2,0)

    cameraNode.subcribeAndWait(cameraNode.SubscribeTopic2)
    

def callbackFromGripperState(data):
    rospy.loginfo("going in")
    print(data.is_moving)
    if(data.is_moving == False):
        print("here is nice")
        cameraNode.sub2.unregister()
        cameraNode.publish(cameraNode.pub1,coordinatesString)
        cameraNode.subcribeAndWait(cameraNode.SubscribeTopic1)

def callbackFromArduino(data):
    cameraNode.unregisterAllSubscribers()
    rospy.loginfo(data)


if __name__ == '__main__':
    #time.sleep(20)
    print("motorPublisher ready")
    coordinates = np.array([30,0,2], int)
    print( "Camera sending: ", coordinates)
    coordinatesString = coordinates.tostring()
    cameraNode = RosNode(
        nodeName="cameraNode",
        subscribeTopic1="doneMoving", 
        callback1=callbackFromDoneMoving, 
        subscribeTopic2="/gripper/state",
        callback2=callbackFromGripperState,
        subscribeTopic3="arduinoPub",
        callback3=callbackFromArduino,
        publishTopic1="motorPositions", 
        publishTopic2="/gripper/command")
    cameraNode.initNode()
    cameraNode.subscribe(cameraNode.SubscribeTopic3)
    cameraNode.publish(cameraNode.pub2, 0)
    cameraNode.publish( cameraNode.pub1, coordinatesString) 
    cameraNode.subcribeAndWait(cameraNode.SubscribeTopic1)