import math

def invkin(xyz):
    
	d1 = 16.8 # cm (height of 2nd joint)
	a1 = 0.0 # (distance along "y-axis" to 2nd joint)
	a2 = 17.3 # (distance between 2nd and 3rd joints)
	d4 = 23.7 # (distance from 3rd joint to gripper center - all inclusive, ie. also 4th joint)
	
	xc = xyz[0]
	yc = xyz[1]
	zc = xyz[2]


	q1 = math.atan2(yc,xc)
	r2 = math.pow(xc - a1*math.cos(q1),2) + math.pow(yc-a1*math.sin(q1),2)
	s = (zc - d1)
	D = (r2 + math.pow(s,2) - math.pow(a2,2) - math.pow(d4,2))/(2*a2*d4)

	q3 = math.atan2(-math.sqrt(1-math.pow(D,2)),D)
	q2 =  (math.atan2(s,math.sqrt(r2)) - math.atan2(d4*math.sin(q3), a2 + d4*math.cos(q3))) - (math.pi/2)
	q4 = 0 # Not implemementing for last motor, 0 for now
	#print("After: Q1: ", q1 ," Q2: ", q2, " Q3: ", q3) #Uncomment this line to print result of inverse kinematic, used for debugging
	#print("X: ", xyz[0], "Y: ", xyz[1], "Z: ", xyz[2])
	return q1,q2,q3,q4 
